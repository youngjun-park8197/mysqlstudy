[ 데이터베이스와 PHP ]
클라이언트 : 웹브라우저로 서버 접속
서버 : 웹서버가 사용자가 요청한 응답을 수신, 애플리케이션을 처리할 수 있는 php 호출, 동작(코드)
  동작하는 코드 -> <?php "[DB]" ... ?>
  php는 코드 내에 DB 제어 코드를 만나는 경우, 데이터베이스에 접속하여 사용자가 요청한 연관된 데이터가 저장되어 있는 데이터 요청 
  결과값을 웹서버로 리턴하여 웹 브라우저로 송신
* php 자체는 정보 저장 기능X, 타 시스템 사용 -> 데이터베이스를 활용, DB 외에 파일에 저장 가능
* DB에 저장하는 이유 = 데이터 저장에 최적화된 소프트웨어이기 때문
DB 제어 언어 : SQL(Structured Query Language) - SQL 문법에 따라 DB 제어 가능

[윈도우에서 MySQL 접속 방법]
Bitnami 디렉토리 -> wampstack-7.3.11-0 디렉토리 -> mysql -> bin -> mysql.exe (mysql 클라이언트 등 확인 가능)
MySQL monitor : 기본적 제공 소프트웨어(번들), 데이터베이스를 SQL로 직접 제어 확인
cmd 창에서 위 mysql 파일이 존재하는 경로로 이동
ex) cd C:\Bitnami\wampstack-7.3.11-0\mysql\bin
